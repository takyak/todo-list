# TODO-LIST

## UI

#### PC 

[![Image from Gyazo](https://i.gyazo.com/51d3e388fc1314e1c16b3e74d82bdf84.png)](https://gyazo.com/51d3e388fc1314e1c16b3e74d82bdf84)
#### Smart Phone
[![Image from Gyazo](https://i.gyazo.com/6d430c5029bfac12cf6789c751aaeb23.png)](https://gyazo.com/6d430c5029bfac12cf6789c751aaeb23)

## Function
・Create To-Do　  
・Edit To-Do　  
・Delete To-Do   
・Move To-Do into "Later" list           
・Complete To-Do  
・Clear All  

## Example
[![Image from Gyazo](https://i.gyazo.com/4830ccab54455d7619e2dcc7eb0de569.gif)](https://gyazo.com/4830ccab54455d7619e2dcc7eb0de569)
[![Image from Gyazo](https://i.gyazo.com/fcf4c068f1fab5f0bc59ba7ea27c371a.gif)](https://gyazo.com/fcf4c068f1fab5f0bc59ba7ea27c371a)
[![Image from Gyazo](https://i.gyazo.com/893b60375b6ef0e1754a8f76dd717907.gif)](https://gyazo.com/893b60375b6ef0e1754a8f76dd717907)