//elements
const clear = document.querySelector(".clear");
const dateElement = document.getElementById("date");
const calcToday = document.getElementById("todos-today");
const calcLater = document.getElementById("todos-later");
const calcComp = document.getElementById("todos-comp");
const list = document.getElementById("list");
const lateList = document.getElementById("late");
const compList = document.getElementById("complate");
const input = document.getElementById("input");
const tabTodo = document.getElementById("tab-todo");
const tabLater = document.getElementById("tab-later");
const tabComp = document.getElementById("tab-comp");
const spTabTodo = document.getElementById("sp-tab-todo");
const spTabLater = document.getElementById("sp-tab-later");
const spTabComp = document.getElementById("sp-tab-comp");

//class name
const CHECK = "fa-check-circle";
const UNCHECK = "fa-circle-thin";
const LINE_THROUGH = "lineThrough";
const BACK = "fa-reply";
const LATE = "fa-share";
const TABCOLOR = "sp-tab-color";

//Show todays date
const options = {weekday: "long", month: "short", day: "numeric"};
const today = new Date();

dateElement.innerHTML = today.toLocaleDateString("en-JP", options);

//variables
let LIST, id;
//add item from localstorage
let data = localStorage.getItem("TODO");

//check if data is not empty
if(data){
  LIST = JSON.parse(data);
  id = LIST.length;
  loadList(LIST);
}else{
  // if data isnt empty
  LIST = [];
  id = 0;
};

// load items to the UI
function loadList(array) {
  array.forEach(function(item) {
    addTodo(item.name, item.id, item.done, item.trash, item.late);
  });
};

//clear the local storage
clear.addEventListener("click", function() {
  localStorage.clear();
  location.reload();
});

//add item to localstorage
localStorage.setItem("TODO", JSON.stringify(LIST));

//add todo
function addTodo(toDo, id, done, trash, late) {

  if(trash){ return; }
  const DONE = done ? CHECK : UNCHECK;
  const LINE = done ? LINE_THROUGH : "";
  const LATER  = late ? BACK : LATE;
  const ARROW = done ? "" : LATER;

  const item = `<li class="item" id="${late}">
                  <i class="fa ${DONE} co" job="complete" id="${id}"></i>
                  <p class="text ${LINE}">${toDo}</p>
                  <input id="editInput${id}" class="input" type="text" value="${toDo}" />
                  <i class="cl fa ${ARROW}" job="later" id="${id}"></i>
                  <i class="fa fa-pencil ed" job="edit" id="${id}"></i>
                  <i class="fa fa-trash-o de" job="delete" id="${id}"></i>
                </li>`
                
  const position = "beforeend";
  
  if(LIST[id].done == true) {
    compList.insertAdjacentHTML(position, item);
  }else if(LIST[id].late == false) { 
    list.insertAdjacentHTML(position, item);
  }else if(LIST[id].late == true){
    lateList.insertAdjacentHTML(position, item);
  };
  
  tabTodoFunc();
  calcTasks();
};

//add an list with enter key
document.addEventListener("keyup", function(event) {
  if(event.keyCode == 13) {
    const toDo = input.value;

    if(toDo){
      LIST.push({
        name : toDo,
        id : id,
        done : false,
        trash : false,
        late : false
      });
      addTodo(toDo, id, false, false, false);
      //add item to localstorage
      localStorage.setItem("TODO", JSON.stringify(LIST));
     
      id++;
    };
    input.value = "";
  };
});

//complete todo

function completeTodo(element) {
  const todo = element.parentNode;
  const laterIcon = element.parentNode.querySelector(".cl");
  element.classList.toggle(CHECK);
  element.classList.toggle(UNCHECK);
  element.parentNode.querySelector(".text").classList.toggle(LINE_THROUGH);

  laterIcon.classList.remove(LATE);
  laterIcon.classList.remove(BACK);
  LIST[element.id].done = LIST[element.id].done ? false : true;
  
  if(LIST[element.id].done == true) {
    const position = "beforeend";
    compList.insertAdjacentHTML(position, todo.outerHTML);
    todo.classList.add('fadeout');
    setTimeout(function(){ 
      todo.remove();
      calcTasks();
    }, 1200);
    
  }else{
    if(LIST[element.id].late == true) {
      laterIcon.classList.add(BACK);
      const position = "beforeend";
      lateList.insertAdjacentHTML(position, todo.outerHTML);
      todo.remove();
      tabLaterFunc();
    }else{
      laterIcon.classList.add(LATE);
      const position = "beforeend";
      list.insertAdjacentHTML(position, todo.outerHTML);
      todo.remove();
      tabTodoFunc();
    }
  };
  calcTasks();
};

//remove todo
function removeTodo(element) {
  element.parentNode.parentNode.removeChild(element.parentNode);
  
  LIST[element.id].trash = true;
  calcTasks();
};

//edit todo
function editTodo(element) {
  const todoItem = element.parentNode.querySelector(".text");
  const todoInput = element.parentNode.querySelector(".input");
 
  todoItem.classList.add("editMode");
  todoInput.classList.add("editMode"); 
  
  todoText = todoItem.innerHTML;
  todoInput.value = todoText;
  
  todoInput.addEventListener("keyup", function(event) {
    
    if(event.keyCode == 13) {
      const editInput = document.getElementById(`editInput${element.id}`).value;
      
      LIST[element.id].name = editInput;    
      todoItem.innerHTML = editInput;   

      todoItem.classList.remove("editMode")
      todoInput.classList.remove("editMode")
      
      localStorage.setItem("TODO", JSON.stringify(LIST)); 
    };
  });
};

// send todos to later list
function lateTodo(element) {
  const todo = element.parentNode;
  const laterIcon = element.parentNode.querySelector(".cl");

  laterIcon.classList.remove(LATE);
  laterIcon.classList.add(BACK);

  const position = "beforeend";
  lateList.insertAdjacentHTML(position, todo.outerHTML);
  LIST[element.id].late = true;
  
  if(LIST[element.id].late == true) {
    todo.remove();
  }
  tabLaterFunc();
  calcTasks();
};

// send back to today's todo list
function backTodo(element) {
  const todo = element.parentNode;
  const laterIcon = element.parentNode.querySelector(".cl");

  laterIcon.classList.remove(BACK);
  laterIcon.classList.add(LATE);
  
  const position = "beforeend";
  list.insertAdjacentHTML(position, todo.outerHTML);

  LIST[element.id].late = false;

  if(LIST[element.id].late == false) {
    todo.remove();
  }
  tabTodoFunc();
  calcTasks();
};

list.eventFunc;
lateList.eventFunc;

function tabTodoFunc() {
  tabTodo.style.background = "#4162f6";
  tabLater.style.background = "#7f91e2";
  tabComp.style.background = "#7f91e2";

  compList.style.display = "none";
  lateList.style.display = "none";
  list.style.display = "block";

  spTabTodo.classList.add("sp-tab-color");
  spTabLater.classList.remove("sp-tab-color");
  spTabComp.classList.remove("sp-tab-color");
};

function tabLaterFunc() {
  tabTodo.style.background = "#7f91e2";
  tabLater.style.background = "#4162f6";
  tabComp.style.background = "#7f91e2";
 
  compList.style.display = "none";
  list.style.display = "none";
  lateList.style.display = "block";

  spTabTodo.classList.remove("sp-tab-color");
  spTabLater.classList.add("sp-tab-color");
  spTabComp.classList.remove("sp-tab-color");
};

function tabCompFunc() {
  tabTodo.style.background = "#7f91e2";
  tabLater.style.background = "#7f91e2";
  tabComp.style.background = "#4162f6";

  list.style.display = "none";
  lateList.style.display = "none";
  compList.style.display = "block";

  spTabTodo.classList.remove("sp-tab-color");
  spTabLater.classList.remove("sp-tab-color");
  spTabComp.classList.add("sp-tab-color");
};

// calucrate todos in the lists
function calcTasks() {
  calcToday.innerHTML = list.getElementsByTagName("li").length;
  calcLater.innerHTML = lateList.getElementsByTagName("li").length;
  calcComp.innerHTML = compList.getElementsByTagName("li").length;
};

//target the items created dynamically
const eventFunc = addEventListener("click", function(event){
  const element = event.target; 
  const elementJob = element.attributes.job.value;

  if(elementJob == "complete") {
    completeTodo(element);
  }else if(elementJob == "delete") {
    removeTodo(element);
  }else if(elementJob == "edit") {
    editTodo(element);
  }else if(elementJob == "later") {
    if(LIST[element.id].late == false){
      lateTodo(element);
    }else if(LIST[element.id].late == true) {
      backTodo(element);
    };
  }else if(elementJob == "tabTodo") {
    tabTodoFunc();
  }else if(elementJob == "tabLater") {
    tabLaterFunc();
  }else if(elementJob == "tabComp") {
    tabCompFunc();
  };

  //add item to localstorage
  localStorage.setItem("TODO", JSON.stringify(LIST)); 
});


